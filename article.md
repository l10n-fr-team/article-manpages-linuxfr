# La traduction des pages de manuel en français a repris du rythme

Voilà très longtemps que la traduction des pages de manuel est un sujet difficile à traiter, tant il y a de paquets, de mises à jour, de pages, etc.

## Historique

* Perkamon
* Traduc.org

{si quelqu'un a le temps de rédiger un historique,  ce serait cool de rappeler comment on a fait, Perkamon, etc]



Bref, après l'abandon de Perkamon, le sujet devenait véritablement très difficile à traiter. Une équipe allemande s'y est pourtant remise, et a fait un travail de mise à jour tout à fait sensationnel, dont elle a fait bénéficier quatre langues. Elle a mis au point un système de scripts qui va récupérer les pages de manuel dans les paquets, qui les traite pour voir leur état, et qui génère les fichiers de langue nécessaires à leur traduction. Sont supportés aujourd'hui le français, l'allemand, le polonais, le portugais. Les créateurs annoncent déjà espérer que d'autres langues les rejoindront, mais les pages de man dans ces langues sont vraiment très en retard et non maintenues.

Le site de contribution de Debian, salsa, héberge aujourd'hui les sources de ce paquet. Pour y contribuer, il suffit de vous créer un compte salsa et de rfécupérer le paquet sur le gitlab.

Debian a mis en place un process pour garantir la qualité et l'homogénéité des traductions, décrit ici:
https://debian-facile.org/doc:mentors:mentors#aider-l-equipe-de-traduction-francophone-debutant-avise

Les modalités de publication sont à chercher dans la racine du paquet, où un fichier contributing.md explique en détails les précautions à prendre, les aides possibles et comment fonctionnent les scripts.

Bien qu'hébergé chez Debian, ces pages de man n'en restent pas moins interdistributions. Typiquement, la page de manuel de kill relève moins de Debian que de distributions comme Archlinux ou Mageia. Toutes les contributions seraient bienvenues.

Les quatre personnes qui y travaillent depuis février ont déjà mis à jour près de 80 pages relevant de la section 1. Il en reste beaucoup, donc les contributions sont forcément les bienvenues. 